import logo from './logo.svg';
import './App.css';
import Header from './cpn/Header';
import Footer from './cpn/Footer';
import Body from './cpn/Body';

function App() {
  return (
    <div className="App">
      <Header />
      
      <header className="App-header">
        <p>
        <Body />
        </p>
        <a href="https://google.com"> Google </a>
      </header>
      <Footer />
    </div>
  );
}

export default App;
