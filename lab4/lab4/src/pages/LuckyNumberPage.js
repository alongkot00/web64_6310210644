import LuckyNumber from '../components/LuckyNumber'
import {useState, useEffect} from "react";

function LuckyNumberPage (){
    /*const [num, setNum] = useState(0);

    useEffect(() => {
        let n = setRandom(() => {
        setNum((num)=> num=53);
        });
    });*/

    const [sentence, setSentence] =  useState("");
    const [num, setNum] = useState();

    function RandomNumber(){
        let n = parseInt(num);
        setNum(n);
        if(n == 53){
            setSentence("เข้.ถูกได้ไง");
        }else if((n>=0) && (n<=99)){
            setSentence("ผิด");
        }else{
            setSentence("ก็บอกว่า0-99ไงอย่าดื้อสิ");
        }
    }

    return (
        <div align = "center">
            <h1>มาสุ่มเลขกัน</h1><br/>
            <h2>กรุณาทายตัวเลขที่ต้องการระหว่าง 0-99 : <input type = "text" value={num} onChange={(e) => { setNum(e.target.value);}}/></h2>
            <button onClick={(e) => {RandomNumber()}}>ทาย</button>

            { num != 0 &&
                <div>
                    <br/>
                    <LuckyNumber
                            random = {sentence}
                    />
                </div>
            }        
        </div>
    );

}


export default LuckyNumberPage;