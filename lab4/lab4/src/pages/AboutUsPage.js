import AboutUs from '../components/AboutUs'

function AboutUsPage(){

    return (
        <div align = "center">
            <br/>
            <h1>ผู้จัดทำเว็บนี้</h1>
            <AboutUs name = "Minikotx"
              province = "Along"/>
            <br/>
        </div>
    ); 
}

export default AboutUsPage;