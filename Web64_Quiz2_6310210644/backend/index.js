const bcrypt = require('bcrypt')
const SALT_ROUNDS = 10

const jwt = require('jsonwebtoken')
const dotenv = require('dotenv')
dotenv.config()
const TOKEN_SECRET = process.env.TOKEN_SECRET

const mysql = require('mysql')

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'admin',
    password: 'admin',
    database: 'MusicSchool'
});

connection.connect();
const express = require('express')
const app = express();
const port = 4000;

/*User Token*/
function authenticateToken(req, res, next) {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    if (token == null) return res.sendStatus(401)
    jwt.verify(token, TOKEN_SECRET, (err, user) => {
        if (err) { return res.sendStatus(403) }
        else {
            req.user = user 
            next()
        }
    })
}

/*SELECT*/
app.get("/list", (req, res) => {

    query = "SELECT * from Sing"

    connection.query(query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                "status": "400",
                "message": "Error inserting data into db"
            })
        } else {
            res.json(rows);
        }
    });
});


/*INSERT INTO*/
app.post("/add_sing", (req, res) => {

    let number = req.query.number
    let name = req.query.name
    let surname = req.query.surname
    let age = req.query.age
    let mobile_number = req.query.mobile_number

    let query = `INSERT INTO Sing
                (Number, Name, Surname, Age, MobileNumber)
                VALUES ('${number}',
                        '${name}',
                        '${surname}',
                        '${age}',
                        '${mobile_number}')`
    console.log(query)

    connection.query(query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                "status": "400",
                "message": "Error inserting data into db"
            })
        } else {
            res.json({
                "status": "200",
                "message": "Adding event succesful"
            })
        }
    });
})


/*UPDATE*/
app.post("/update", (req, res) => {
    
    let update_id = req.query.update_id
    let update_course = req.query.update_course
    let update_hour = req.query.update_hour

    let query = `UPDATE Course SET
                      Course =' ${update_course}', 
                      Hour = '${update_hour}'
                      WHERE CourseID = '${update_id}'`
    console.log(query)

    connection.query(query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                "status": "400",
                "message": "Error updating record"
            })
        } else {
            res.json({
                "status": "200",
                "message": "Updating event succesful"
            })
        }
    });
})


/*DELETE*/
app.post("/delete", (req, res) => {

    let delete_number = req.query.delete_number

    let query = `DELETE FROM Course
                   WHERE CourseID = '${delete_number}'`
    console.log(query)

    connection.query(query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                "status": "400",
                "message": "Error delete record"
            })
        } else {
            res.json({
                "status": "200",
                "message": "Deleting record success"
            })
        }
    });
})


/*register*/
app.post("/register", authenticateToken, (req, res) => {
    
    let user_profile = req.user
    let register_name = req.query.register_name
    let register_surname = req.query.register_surname
    let register_age = req.query.register_age
    let register_mobile_number = req.query.register_mobile_number

    let register_username = req.query.register_username
    let register_password = req.query.register_password

    bcrypt.hash(register_password, SALT_ROUNDS, (err, hash) => {
        let query = `INSERT INTO Sing
                (Name, Surname, Age, MobileNumber, Username, Password, IsAdmin)
                VALUES ('${register_name}',
                        '${register_surname}',
                        '${register_age}',
                        '${register_mobile_number}',
                        '${register_username}',
                        '${hash}', false )`

        console.log(query)

        connection.query(query, (err, rows) => {
            if (err) {
                console.log(err)
                res.json({
                    "status": "400",
                    "message": "Error inserting data into db"
                })
            } else {
                res.json({
                    "status": "200",
                    "message": "Adding new user succesful"
                })
            }
        })

    });
});

/*login */
app.post("/login", (req, res) => {

    let username = req.query.username
    let password = req.query.password

    let query = `SELECT * FROM Sing
                 WHERE Username = '${username}'`
    connection.query(query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                "status": "400",
                "message": "Error querying from running db"
            })
        } else {
            let db_password = rows[0].Password
            bcrypt.compare(password, db_password, (err, result) => {
                if (result) {
                    let payload = {
                        "username": rows[0].Username,
                        "user_id": rows[0].Number,
                        "IsAdmin": rows[0].IsAdmin
                    }
                    console.log(payload)
                    let token = jwt.sign(payload, TOKEN_SECRET, { expiresIn : '1d' })
                    res.send(token)

                } else { res.send("Invalid username / password") }
            })
        }
    })

})

app.listen(port, () => {
    console.log(`Now starting Running System Backend ${port}`)
})
