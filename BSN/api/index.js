const bcrypt = require('bcrypt')
const SALT_ROUNDS = 10

const jwt = require('jsonwebtoken')
const dotenv = require('dotenv')
dotenv.config()
const TOKEN_SECRET = process.env.TOKEN_SECRET

const mysql = require('mysql')

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'admin',
    password: '123456',
    database: 'RunningSystem'
});

connection.connect();
const express = require('express')
const app = express();
const port = 4000;

/*User Token*/

function authenticateToken(req, res, next) {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    if (token == null) return res.sendStatus(401)
    jwt.verify(token, TOKEN_SECRET, (err, user) => {
        if (err) { return res.sendStatus(403) }
        else {
            req.user = user 
            next()
        }
    })
}

/*
List All 1
 Runner.RunnerID, Runner.RunnerName, Runner.RunnerSurname, RunnerEvent.EventName, 
 Registation.Distance, Registation.RegistrationTime 
 FROM Runner, Registation, RunnerEvent 
 WHERE (Registation.RunnerID = Runner.RunnerID) AND 
       (Registation.EventID = RunnerEvent.EventID);
*/

app.get("/list_registation", (req, res) => {
    query = `
    SELECT Runner.RunnerID, Runner.RunnerName, Runner.RunnerSurname, RunnerEvent.EventName, 
           Registation.Distance, Registation.RegistrationTime 

    FROM Runner, Registation, RunnerEvent 

    WHERE (Registation.RunnerID = Runner.RunnerID) AND 
          (Registation.EventID = RunnerEvent.EventID);`

    connection.query(query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                "status": "400",
                "message": "Error querying from running db"
            })
        } else {
            res.json(rows);
        }
    });
})

/*
List All 2 runner event
Runner.RunnerID, Runner.RunnerName, Runner.RunnerSurname, RunnerEvent.EventName, 
Registation.Distance, Registation.RegistrationTime 
FROM Runner, Registation, RunnerEvent 
WHERE (Registation.RunnerID = Runner.RunnerID) AND 
      (Registation.EventID = RunnerEvent.EventID) AND 
      (RunnerEvent.EventID = 1);
*/
app.get("/list_reg_event", (req, res) => {

    let event_id = req.query.event_id
    let query = `
    SELECT Runner.RunnerID, Runner.RunnerName, Runner.RunnerSurname, 
           Registation.Distance, Registation.RegistrationTime 

    FROM Runner, Registation, RunnerEvent 

    WHERE (Registation.RunnerID = Runner.RunnerID) AND 
          (Registation.EventID = RunnerEvent.EventID) AND 
          (RunnerEvent.EventID = ${event_id});`

    connection.query(query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                "status": "400",
                "message": "Error querying from running db"
            })
        } else {
            res.json(rows);
        }
    });
})

/*
 List All 3 runner ID
 Runner.RunnerID, RunnerEvent.EventName, Registation.Distance, Registation.RegistrationTime 
 FROM Runner, RunnerEvent, Registation 
 WHERE (Runner.RunnerID = Registation.RunnerID) AND 
       (Registation.EventID = RunnerEvent.EventID) AND 
       (Runner.RunnerID = 2);
 */
app.get("/list_reg_event_runnerid", authenticateToken, (req, res) => {
    
    let runner_id = req.user.user_id
   
    let query = `
        SELECT Runner.RunnerID, Runner.RunnerName, Runner.RunnerSurname, 
               Registation.Distance, Registation.RegistrationTime 
    
        FROM Runner, Registation, RunnerEvent 
    
        WHERE (Registation.RunnerID = Runner.RunnerID) AND 
              (Registation.EventID = RunnerEvent.EventID) AND 
              (Runner.RunnerID = ${runner_id});`

    connection.query(query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                "status": "400",
                "message": "Error querying from running db"
            })
        } else {
            res.json(rows);
        }
    });
})
/*register event*/
app.post("/register_event", authenticateToken, (req, res) => {

    let user_profile = req.user
    let runner_id = req.user.user_id
    let event_id = req.query.event_id
    let distance = req.query.distance

    let query = `INSERT INTO Registation
                (RunnerID, EventID, Distance, RegistrationTime)
                VALUES ('${runner_id}',
                        '${event_id}',
                        '${distance}',
                         NOW() )`
    console.log(query)

    connection.query(query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                "status": "400",
                "message": "Error inserting data into db"
            })
        } else {
            res.json({
                "status": "200",
                "message": "Adding event succesful"
            })
        }
    });
});

/*API login */
app.post("/login", (req, res) => {

    let username = req.query.username
    let password = req.query.password
    let query = `SELECT * FROM Runner
                 WHERE Username = '${username}'`
    connection.query(query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                "status": "400",
                "message": "Error querying from running db"
            })
        } else {
            let add_password = rows[0].Password
            bcrypt.compare(password, add_password, (err, result) => {
                if (result) {
                    let payload = {
                        "username": rows[0].Username,
                        "user_id": rows[0].RunnerID,
                        "IsAdmin": rows[0].IsAdmin
                    }
                    console.log(payload)
                    let token = jwt.sign(payload, TOKEN_SECRET, { expiresIn: '1d' })
                    res.send(token)
                } else { res.send("Invalid username / password") }
            })
        }
    })

})

/*register runner*/
app.post("/register_runner", (req, res) => {

    let runner_name = req.query.runner_name
    let runner_surname = req.query.runner_surname
    let runner_username = req.query.runner_username
    let runner_password = req.query.runner_password

    bcrypt.hash(runner_password, SALT_ROUNDS, (err, hash) => {
        let query = `INSERT INTO Runner
                (RunnerName, RunnerSurname, Username, Password, IsAdmin)
                VALUES ('${runner_name}',
                        '${runner_surname}',
                        '${runner_username}',
                        '${hash}', false )`

        console.log(query)

        connection.query(query, (err, rows) => {
            if (err) {
                console.log(err)
                res.json({
                    "status": "400",
                    "message": "Error inserting data into db"
                })
            } else {
                res.json({
                    "status": "200",
                    "message": "Adding new user succesful"
                })
            }
        })


    });
});


/*update runner*/
app.post("/update_runner", (req, res) => {

    let runner_name = req.query.runner_name
    let runner_surname = req.query.runner_surname

    let query = `INSERT INTO Runner
    (RunnerName, RunnerSurname)
    VALUES ('${runner_name}','${runner_surname}')`
    console.log(query)

    connection.query(query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                "status": "400",
                "message": "Error updating record"
            })
        } else {
            res.json({
                "status": "200",
                "message": "Updating event succesful"
            })
        }
    });
})


/*delete runner*/
app.post("/delete_runner", (req, res) => {

    let runner_name = req.query.runner_name
    let runner_surname = req.query.runner_surname

    let query = `INSERT INTO Runner
    (RunnerName, RunnerSurname)
    VALUES ('${runner_name}','${runner_surname}')`
    console.log(query)

    connection.query(query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                "status": "400",
                "message": "Error delete record"
            })
        } else {
            res.json({
                "status": "200",
                "message": "Deleting record success"
            })
        }
    });
})


/*Runner Event*/
app.get("/list_event", (req, res) => {
    query = "SELECT * from RunnerEvent";
    connection.query(query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                "status": "400",
                "message": "Error querying from running db"
            })
        } else {
            res.json(rows);
        }
    });
})


/*add Event*/
app.post("/add_event", (req, res) => {
    let event_name = req.query.event_name
    let event_location = req.query.event_location

    let query = `INSERT INTO RunnerEvent
                (EventName, EventLocation)
                VALUES ('${event_name}','${event_location}')`
    console.log(query)

    connection.query(query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                "status": "400",
                "message": "Error inserting data into db"
            })
        } else {
            res.json({
                "status": "200",
                "message": "Adding event succesful"
            })
        }
    });
})


/*update Event*/
app.post("/update_event", (req, res) => {

    let event_id = req.query.event_id
    let event_name = req.query.event_name
    let event_location = req.query.event_location

    let query = `UPDATE RunnerEvent SET
                      EventName =' ${event_name}', 
                      EventLocation = '${event_location}'
                      WHERE EventID = '${event_id}'`
    console.log(query)

    connection.query(query, (err, rows) => {
        if (err) {
            console.log(err)
            console.log(err)
            res.json({
                "status": "400",
                "message": "Error updating record"
            })
        } else {
            res.json({
                "status": "200",
                "message": "Updating event succesful"
            })
        }
    });
})


/*delete Event*/
app.post("/delete_event", (req, res) => {

    let event_id = req.query.event_id

    let query = `DELETE FROM RunnerEvent
                      WHERE EventID = '${event_id}'`
    console.log(query)

    connection.query(query, (err, rows) => {
        if (err) {
            console.log(err)
            console.log(err)
            res.json({
                "status": "400",
                "message": "Error delete record"
            })
        } else {
            res.json({
                "status": "200",
                "message": "Deleting record success"
            })
        }
    });
})









app.listen(port, () => {
    console.log(`Now starting Running System Backend ${port}`)
})



/* query = "SELECT * from Runner";
connection.query( query, (err, rows) =>{
    if (err) {
        console.log(err);
    }else {
        console.log(rows);
    }
});

connection.end(); */